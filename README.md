# xAPI Definitions

A repository to host definitions for xAPI verbs, activities and other related features.

Structure used from https://gitlab.com/learntech-rwth/xapi 

If we take their definitions we have to include the license! 